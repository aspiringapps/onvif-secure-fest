package com.example.hp.mayukhapp;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class Maps extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap1 = googleMap;

        // Add a marker in Sydney and move the camera
        mMap1 = googleMap;
        mMap1.setMaxZoomPreference(20.0f);
        mMap1.setMinZoomPreference(14.0f);

        LatLng it = new LatLng(26.4029814, 75.8701285);
        mMap1.addMarker(new MarkerOptions().position(it).title("I.T center").icon(BitmapDescriptorFactory.fromResource((R.drawable.code_))));
        mMap1.moveCamera(CameraUpdateFactory.newLatLng(it));LatLng lib = new LatLng(26.4029820, 75.8701300);
        mMap1.addMarker(new MarkerOptions().position(lib).title("LIBRARY"));
        mMap1.moveCamera(CameraUpdateFactory.newLatLng(lib));
        LatLng vm = new LatLng(26.40348695,75.87302887);
        mMap1.addMarker(new MarkerOptions().position(vm).title("VANI MANDIR"));
        mMap1.moveCamera(CameraUpdateFactory.newLatLng(vm));
        LatLng AIM = new LatLng(26.40216482,75.8756621);
        mMap1.addMarker(new MarkerOptions().position(AIM).title("EVENTS").icon(BitmapDescriptorFactory.fromResource((R.drawable.competition))));
        mMap1.moveCamera(CameraUpdateFactory.newLatLng(AIM));
        LatLng SHANUZ = new LatLng(26.40137128, 75.87562777);
        mMap1.addMarker(new MarkerOptions().position(SHANUZ).title("SHANUZ").icon(BitmapDescriptorFactory.fromResource((R.drawable.fastfood_map))));
        mMap1.moveCamera(CameraUpdateFactory.newLatLng(SHANUZ));
        LatLng NM = new LatLng(26.4130121, 75.87558871);
        mMap1.addMarker(new MarkerOptions().position(NM).title("NEW MARKET").icon(BitmapDescriptorFactory.fromResource((R.drawable.restaurant_map))));
        mMap1.moveCamera(CameraUpdateFactory.newLatLng(NM));
        LatLng spicy = new LatLng(26.401076, 75.8735472);
        mMap1.addMarker(new MarkerOptions().position(spicy).title("SPICY BITE").icon(BitmapDescriptorFactory.fromResource((R.drawable.fastfood_map))));
        mMap1.moveCamera(CameraUpdateFactory.newLatLng(spicy));
        LatLng AYATAN = new LatLng(26.401000, 75.8754120);
        mMap1.addMarker(new MarkerOptions().position(AYATAN).title("SHANTA AYTAN").icon(BitmapDescriptorFactory.fromResource((R.drawable.hos))));
        mMap1.moveCamera(CameraUpdateFactory.newLatLng(AYATAN));
        LatLng WISDOM = new LatLng(26.40142209, 75.87617187);
        mMap1.addMarker(new MarkerOptions().position(WISDOM).title("WISDOM").icon(BitmapDescriptorFactory.fromResource((R.drawable.competitions_map))));
        mMap1.moveCamera(CameraUpdateFactory.newLatLng(WISDOM));
        LatLng GH = new LatLng(26.40061153, 75.87774126);
        mMap1.addMarker(new MarkerOptions().position(GH).title("GUEST HOUSE").icon(BitmapDescriptorFactory.fromResource((R.drawable.hos))));
        mMap1.moveCamera(CameraUpdateFactory.newLatLng(GH));
        LatLng PO= new LatLng(26.39936143, 75.88028692);
        mMap1.addMarker(new MarkerOptions().position(PO).title("POST OFFICE").icon(BitmapDescriptorFactory.fromResource((R.drawable.postofffice))));
        mMap1.moveCamera(CameraUpdateFactory.newLatLng(PO));
        LatLng GATE= new LatLng(26.3975463, 75.8885165);
        mMap1.addMarker(new MarkerOptions().position(GATE).title("ENTRANCE GATE "));
        mMap1.moveCamera(CameraUpdateFactory.newLatLng(GATE));
        LatLng UCOBK = new LatLng(26.39947743, 75.8801829);
        mMap1.addMarker(new MarkerOptions().position(UCOBK).title("UCO BANK").icon(BitmapDescriptorFactory.fromResource((R.drawable.hos))));
        mMap1.moveCamera(CameraUpdateFactory.newLatLng(UCOBK));
        LatLng SBI = new LatLng(26.39956587, 75.88005581);
        mMap1.addMarker(new MarkerOptions().position(SBI).title("SBI").icon(BitmapDescriptorFactory.fromResource((R.drawable.hos))));
        mMap1.moveCamera(CameraUpdateFactory.newLatLng(SBI));
        LatLng AROGYA = new LatLng(26.39959596, 75.87928685);
        mMap1.addMarker(new MarkerOptions().position(AROGYA).title("AROGYA MANDIR"));
        mMap1.moveCamera(CameraUpdateFactory.newLatLng(AROGYA));
        LatLng SAUD= new LatLng(26.40122859, 75.87627413);
        mMap1.addMarker(new MarkerOptions().position(SAUD).title("SHANTA SAUDH").icon(BitmapDescriptorFactory.fromResource((R.drawable.hos))));
        mMap1.moveCamera(CameraUpdateFactory.newLatLng(SAUD));
        LatLng NIK = new LatLng(26.40149575, 75.87389828);
        mMap1.addMarker(new MarkerOptions().position(NIK).title("SHANTA NIKETAN").icon(BitmapDescriptorFactory.fromResource((R.drawable.hos))));
        mMap1.moveCamera(CameraUpdateFactory.newLatLng(NIK));


    }
}
