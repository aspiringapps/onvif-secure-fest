package com.example.hp.mayukhapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

/**
 * Created by hp on 25-02-2018.
 */

public class CustomAdapterec extends BaseAdapter {

    Context context;
    int logos[];
    LayoutInflater inflator;

    public CustomAdapterec(Context applicationContext, int[] logos) {
        this.context = applicationContext;
        this.logos = logos;
        inflator = (LayoutInflater.from(applicationContext));

    }
    @Override
    public int getCount(){
        return logos.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup){
        view = inflator.inflate(R.layout.activity_grid_view, null);
        ImageView icon = (ImageView) view.findViewById(R.id.icon);
        icon.setImageResource(logos[i]);

        return view;
    }
}


