package com.example.hp.mayukhapp;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Created by hp on 18-02-2018.
 */

public class ImageAdapter extends PagerAdapter{



    Context context;
    private int[] GalImages = new int[]{
            R.drawable.mayukhapp,R.drawable.gz,
            R.drawable.ga,
            R.drawable.gb,
            R.drawable.gc,
            R.drawable.gd,
            R.drawable.go,
            R.drawable.ge,
            R.drawable.gf,
            R.drawable.gg,
            R.drawable.gh,
            R.drawable.gi,
            R.drawable.gj,
            R.drawable.gk,
            R.drawable.gl,
            R.drawable.gm,
            R.drawable.gn,
            R.drawable.gp,
            R.drawable.gallerym
                };





    public ImageAdapter(Context context) {
        this.context = context;


    }





    @Override
    public int getCount() {
        return GalImages.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int pos) {

        LayoutInflater inflater = ((Activity)context).getLayoutInflater();

        View viewItem = inflater.inflate(R.layout.image_item, container, false);
        ImageView imageView = (ImageView)viewItem.findViewById(R.id.imageView);
        imageView.setImageResource(GalImages[pos]);
        // TextView textView = (TextView)viewItem.findViewById((R.id.textView));
        //textView.setText("hi");
        ((ViewPager)container).addView(viewItem);
        return  viewItem;
    }
    @Override
    public void destroyItem(ViewGroup container,int pos,Object obj){
        ((ViewPager)container).removeView((View) obj);
    }
}
