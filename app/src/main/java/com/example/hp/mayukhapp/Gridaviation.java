package com.example.hp.mayukhapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

public class Gridaviation extends AppCompatActivity {
    GridView simplegrid;
    int logos[] = {R.drawable.technostark, R.drawable.aviatorss,R.drawable.high};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gridaviation);


        simplegrid = (GridView) findViewById(R.id.simpleGridView);
        CustomAdapterec customAdapter = new CustomAdapterec(getApplicationContext(), logos);
        simplegrid.setAdapter(customAdapter);

        simplegrid.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {

                        if (pos == 0) {
                            Intent in = new Intent(Gridaviation.this, technostork.class);
                            //in.putExtra("image",logos[pos]);
                            startActivity(in);
                        }
                        if (pos == 1) {
                            Intent ip = new Intent(Gridaviation.this, aviators.class);
                            startActivity(ip);
                        }
                        if (pos == 2) {
                            Intent ip = new Intent(Gridaviation.this, thehighnmighty.class);
                            startActivity(ip);
                        }
                    }

                });
    }
}