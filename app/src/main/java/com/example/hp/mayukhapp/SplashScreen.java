package com.example.hp.mayukhapp;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class SplashScreen extends AppCompatActivity {

    ImageView imageView;
    MediaPlayer mp;




        @Override
        public void onCreate(Bundle icicle) {
            super.onCreate(icicle);
            setContentView(R.layout.activity_splash_screen);

            imageView = (ImageView) findViewById(R.id.imageSplash);
            mp = MediaPlayer.create(SplashScreen.this, R.raw.song);
            Animation an2= AnimationUtils.loadAnimation(this,R.anim.bounce);


            Thread t=new Thread()
            {
                @Override
                public void run() {
                    super.run();
                    try {
                        mp.start();
                        sleep(5000);
                        mp.pause();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    finally {

                        finish();
                    }
                }
            };
            t.start();

            imageView.startAnimation(an2);
            int SPLASH_DISPLAY_LENGTH = 2500;


            new Handler().postDelayed(new Runnable(){
                @Override
                public void run() {




                    Intent mainIntent = new Intent(SplashScreen.this,HomEvents.class);
                 SplashScreen.this.startActivity(mainIntent);
                   SplashScreen.this.finish();
                }
            }, SPLASH_DISPLAY_LENGTH);
        }
    }











