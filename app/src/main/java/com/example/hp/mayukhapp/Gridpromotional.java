package com.example.hp.mayukhapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

public class Gridpromotional extends AppCompatActivity {

    GridView simplegrid;
    int logos[] = {R.drawable.shadowclick,R.drawable.givehand,R.drawable.megacrossword};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gridpromotional);


        simplegrid = (GridView) findViewById(R.id.simpleGridView);
        CustomAdapter customAdapter = new CustomAdapter(getApplicationContext(), logos);
        simplegrid.setAdapter(customAdapter);

        simplegrid.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {

                        if (pos == 0) {
                            Intent in = new Intent(Gridpromotional.this, shadowclick.class);
                            //in.putExtra("image",logos[pos]);
                            startActivity(in);
                        }
                        if (pos == 1) {
                            Intent in = new Intent(Gridpromotional.this, givehands.class);
                            //in.putExtra("image",logos[pos]);
                            startActivity(in);
                        }
                        if (pos == 2) {
                            Intent in = new Intent(Gridpromotional.this, megacrossword.class);
                            //in.putExtra("image",logos[pos]);
                            startActivity(in);
                        }
                    }

                });
    }
}


