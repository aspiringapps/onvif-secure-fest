package com.example.hp.mayukhapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

public class GridDemo extends AppCompatActivity {

    GridView simplegrid;
    int logos[] = {R.drawable.ware, R.drawable.cybo, R.drawable.getset, R.drawable.metadatacs};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_demo);


        simplegrid = (GridView)findViewById(R.id.simpleGridView);
        CustomAdapter customAdapter = new CustomAdapter(getApplicationContext(), logos);
        simplegrid.setAdapter(customAdapter);
        simplegrid.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {

                        if(pos==0) {
                            Intent in = new Intent(GridDemo.this, TechWizard.class);
                            //in.putExtra("image",logos[pos]);
                            startActivity(in);
                        }
                        if(pos == 1){
                            Intent ip = new Intent(GridDemo.this,CyborgActivity.class);
                            startActivity(ip);
                        }
                        if(pos == 2){
                            Intent in = new Intent(GridDemo.this,GetSetAlgo.class);
                            startActivity(in);
                        }

                        if(pos == 3){
                            Intent in = new Intent(GridDemo.this,MetaData.class);
                            startActivity(in);
                        }
                    }
                });

    }
    }

