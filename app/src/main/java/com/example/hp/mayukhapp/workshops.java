package com.example.hp.mayukhapp;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class workshops extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workshops);
    }

    public  void gotoinhouse(View v)
    {
        Intent i= new Intent(this,Inhouseworkshops.class);
        startActivity(i);

    }
    public  void gotoouthouse(View V)
    {
        Intent i=new Intent(this,Outhouseworkshops.class);
        startActivity(i);
    }
    public void workshopschedule(View V)
    {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://drive.google.com/file/d/17g7FVd6fe8mkDbYHNRhoR3dx7AfQpGgO/view?usp=drivesdk")));
    }
}
