package com.example.hp.mayukhapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

public class Gridotheractivity extends AppCompatActivity {
    GridView simplegrid;
    int logos[] = {R.drawable.questa,R.drawable.artfist,R.drawable.ruins,R.drawable.worth,R.drawable.legale,
            R.drawable.castles,R.drawable.combatants,R.drawable.encompassed,R.drawable.socialb,R.drawable.ihuntt,R.drawable.gll};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gridotheractivity);

        simplegrid = (GridView) findViewById(R.id.simpleGridView);
        CustomAdapterec customAdapter = new CustomAdapterec(getApplicationContext(), logos);
        simplegrid.setAdapter(customAdapter);

        simplegrid.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {

                        if (pos == 0) {
                            Intent in = new Intent(Gridotheractivity.this, questa.class);
                            //in.putExtra("image",logos[pos]);
                            startActivity(in);
                        }


                        if (pos == 1) {
                            Intent in = new Intent(Gridotheractivity.this, hastkalakar.class);
                            //in.putExtra("image",logos[pos]);
                            startActivity(in);
                        }
                        if (pos == 2) {
                            Intent in = new Intent(Gridotheractivity.this, Kavi.class);
                            //in.putExtra("image",logos[pos]);
                            startActivity(in);
                        }
                        if (pos == 3) {
                            Intent in = new Intent(Gridotheractivity.this,vocab.class);
                            //in.putExtra("image",logos[pos]);
                            startActivity(in);
                        }
                        if (pos == 4) {
                            Intent in = new Intent(Gridotheractivity.this, Legalenigma.class);
                            //in.putExtra("image",logos[pos]);
                            startActivity(in);
                        }
                        if (pos == 5) {
                            Intent in = new Intent(Gridotheractivity.this,AstroQuest.class);
                            //in.putExtra("image",logos[pos]);
                            startActivity(in);
                        }
                        if (pos == 6) {
                            Intent in = new Intent(Gridotheractivity.this, Combatant.class);
                            //in.putExtra("image",logos[pos]);
                            startActivity(in);
                        }

                        if (pos == 7) {
                            Intent in = new Intent(Gridotheractivity.this, Tarkvitark.class);
                            //in.putExtra("image",logos[pos]);
                            startActivity(in);
                        }
                        if (pos == 8) {
                            Intent in = new Intent(Gridotheractivity.this, Socialbonanza.class);
                            //in.putExtra("image",logos[pos]);
                            startActivity(in);
                        }
                        if (pos == 9) {
                            Intent in = new Intent(Gridotheractivity.this, IHUNT.class);
                            //in.putExtra("image",logos[pos]);
                            startActivity(in);
                        }
                        if (pos == 10) {
                            Intent in = new Intent(Gridotheractivity.this, langamng.class);
                            //in.putExtra("image",logos[pos]);
                            startActivity(in);
                        }

                    }

                });
    }



}
