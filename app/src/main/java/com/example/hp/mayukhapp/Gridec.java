package com.example.hp.mayukhapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

public class Gridec extends AppCompatActivity {
    GridView simplegrid;
    int logos[] = {R.drawable.madcad, R.drawable.iseeic,R.drawable.gridtonics, R.drawable.baffle, R.drawable.circuitr};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gridec);

        simplegrid = (GridView) findViewById(R.id.simpleGridView);
        CustomAdapter customAdapter = new CustomAdapter(getApplicationContext(), logos);
        simplegrid.setAdapter(customAdapter);

        simplegrid.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {

                        if (pos == 0) {
                            Intent in = new Intent(Gridec.this, Madcad.class);
                            //in.putExtra("image",logos[pos]);
                            startActivity(in);
                        }
                        if (pos == 1) {
                            Intent ip = new Intent(Gridec.this, iseeic.class);
                            startActivity(ip);
                        }
                        if (pos == 2) {
                            Intent in = new Intent(Gridec.this, gridotonics.class);
                            startActivity(in);
                        }

                        if (pos == 3) {
                            Intent in = new Intent(Gridec.this, bafflecircuit.class);
                            startActivity(in);
                        }
                        if (pos == 4) {
                            Intent in = new Intent(Gridec.this, Curcuitrix.class);
                            startActivity(in);
                        }


                    }

                });


    }
}


