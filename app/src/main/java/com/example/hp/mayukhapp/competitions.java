package com.example.hp.mayukhapp;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class competitions extends AppCompatActivity   {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_competitions);
    }
    public void GoToCS(View v){
        Intent in = new Intent(this,GridDemo.class);
        startActivity(in);
    }
    public  void gotoec(View V)
    {
        Intent in=new Intent(this,Gridec.class);
        startActivity(in);
    }
public void gotoPhysics(View v)
{
    Intent i=new Intent(this,gridphysics.class);
    startActivity(i);
}

    public void gotoaviation(View V)
    {
        Intent i=new Intent(this,Gridaviation.class);
        startActivity(i);
    }
    public void gotophotoshop(View V)
    {
        Intent i=new Intent(competitions.this,gridphotoshop.class);
        startActivity(i);
    }
    public  void gotomaths(View v)
    {
        Intent i=new Intent(competitions.this,Gridmaths.class);
        startActivity(i);

    }
    public void gotostats(View v)
    {
        Intent i=new Intent(competitions.this,Gridstats.class);
        startActivity(i);
    }
     public void gotopromotional(View v)
     {
         Intent i=new Intent(competitions.this,Gridpromotional.class);
         startActivity(i);
     }
    public void gotosubmission(View v)
    {
        Intent i=new Intent(competitions.this,Gridsubmission.class);
        startActivity(i);
    }
    public  void gotoother(View V)
    {
        Intent i=new Intent(competitions.this,Gridotheractivity.class);
        startActivity(i);
    }

public void timetable(View V)
{

    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://drive.google.com/file/d/1dvmlQ32hj5bFUVTbSR1FsA8OD5AlLIhb/view?usp=drivesdk")));
}
}