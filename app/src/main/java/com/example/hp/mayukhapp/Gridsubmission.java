package com.example.hp.mayukhapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

public class Gridsubmission extends AppCompatActivity {

    GridView simplegrid;
    int logos[] = {R.drawable.escape,R.drawable.mash,R.drawable.epoch,R.drawable.technofic,R.drawable.comicologyy,
            R.drawable.hast,R.drawable.innovativemind,R.drawable.armofachelous,R.drawable.roborush,R.drawable.planechallenge,R.drawable.bricabrac};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gridsubmission);


        simplegrid = (GridView) findViewById(R.id.simpleGridView);
        CustomAdapter customAdapter = new CustomAdapter(getApplicationContext(), logos);
        simplegrid.setAdapter(customAdapter);

        simplegrid.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {


                        if (pos == 0) {
                            Intent in = new Intent(Gridsubmission.this, escape.class);
                            //in.putExtra("image",logos[pos]);
                            startActivity(in);
                        }
                        if (pos == 1) {
                            Intent in = new Intent(Gridsubmission.this, Mashupmania.class);
                            //in.putExtra("image",logos[pos]);
                            startActivity(in);
                        }

                        if (pos == 2) {
                            Intent in = new Intent(Gridsubmission.this, epoch.class);
                            //in.putExtra("image",logos[pos]);
                            startActivity(in);
                        }
                        if (pos == 3) {
                            Intent in = new Intent(Gridsubmission.this, Technofiction.class);
                            //in.putExtra("image",logos[pos]);
                            startActivity(in);
                        }
                        if (pos == 4) {
                            Intent in = new Intent(Gridsubmission.this, COMICOLOGY.class);
                            //in.putExtra("image",logos[pos]);
                            startActivity(in);
                        }

                        if (pos == 5) {
                            Intent in = new Intent(Gridsubmission.this, Hastlipi.class);
                            //in.putExtra("image",logos[pos]);
                            startActivity(in);
                        }
                        if (pos == 6) {
                            Intent in = new Intent(Gridsubmission.this, INNOVATIVEMIND.class);
                            //in.putExtra("image",logos[pos]);
                            startActivity(in);
                        }
                        if (pos == 7) {
                            Intent in = new Intent(Gridsubmission.this, armofarchelous.class);
                            //in.putExtra("image",logos[pos]);
                            startActivity(in);
                        }
                        if (pos == 8) {
                            Intent in = new Intent(Gridsubmission.this, roborush.class);
                            //in.putExtra("image",logos[pos]);
                            startActivity(in);
                        }
                        if (pos == 9) {
                            Intent in = new Intent(Gridsubmission.this,planechallenge.class);
                            //in.putExtra("image",logos[pos]);
                            startActivity(in);
                        }

                        if (pos == 10) {
                            Intent in = new Intent(Gridsubmission.this,bricbrac.class);
                            //in.putExtra("image",logos[pos]);
                            startActivity(in);
                        }

                    }

                });
    }
}


