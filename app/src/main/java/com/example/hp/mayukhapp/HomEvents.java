package com.example.hp.mayukhapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class HomEvents extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hom_events);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.hom_events, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
      /*  if (id == R.id.action_settings) {

            Intent i=new Intent(HomEvents.this,Videoplay.class);
            startActivity(i);
            return true;
        }
*/
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {

        } else if (id == R.id.nav_schedule) {

            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://drive.google.com/open?id=1aosGRrJ3g9lu6M_bkOLXujd19UDpunNb")));

        } else if (id == R.id.nav_sponsors) {
            Intent i=new Intent(HomEvents.this,sponsors_mayukh.class);
            startActivity(i);
        } else if (id == R.id.nav_faq) {
            Intent i= new Intent(HomEvents.this,faqs.class);
            startActivity(i);

        } else if (id == R.id.nav_contact) {
            Intent i=new Intent(HomEvents.this,contact.class);
            startActivity(i);

        } else if (id == R.id.nav_developer) {
            Intent i=new Intent(HomEvents.this,Developers.class);
            startActivity(i);

        }
        else if(id==R.id.nav_aboutus)
        {
            Intent i=new Intent(HomEvents.this,Aboutus.class);
            startActivity(i);
        }
        else if(id==R.id.nav_map)
        {
            Intent i=new Intent(HomEvents.this,Maps.class);
            startActivity(i);
        }

else if(id==R.id.nav_ONVIF)
        {
            Intent i=new Intent(HomEvents.this,MainActivity.class);
            startActivity(i);
        }
        else if(id==R.id.nav_univ)
        {
            Intent i=new Intent(HomEvents.this,aboutbanasthali.class);
            startActivity(i);
        }else if(id == R.id.nav_gallery){
            Intent in = new Intent(HomEvents.this,GalleryMa.class);
            startActivity(in);
        }
       /* else if(id == R.id.nav_fb){

            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/mayukh.2k18"));
            startActivity(browserIntent);
        }

        else if(id == R.id.nav_insta){

            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/mayukh.2k18"));
            startActivity(browserIntent);
        }*/

        else if(id == R.id.nav_course){

            Intent in = new Intent(HomEvents.this,Courses.class);
            startActivity(in);
        }

        else if (id == R.id.nav_feedback) {


                //Log.i("Send email", "");

                String[] TO = {"mayukhappteam2018@gmail.com"};
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.setType("text/plain");


                emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
              //  emailIntent.putExtra(Intent.EXTRA_CC, CC);
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Your subject");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message goes here");

                try {
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                    //finish();
                    //Log.i("Fimnished sending email...", "");
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(HomEvents.this,
                            "There is no email client installed.", Toast.LENGTH_SHORT).show();

                }


        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void gotocomp(View v)
    {
        Intent i=new Intent(HomEvents.this,competitions.class);
        startActivity(i);
    }
    public  void gotoworkshops(View V)
    {
        Intent i=new Intent(HomEvents.this,workshops.class);
        startActivity(i);
    }

    public void registerteam(View V)
    {

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(""));
        startActivity(browserIntent);
    }
    public void gotoculturalevents(View V)
    {
        Intent i=new Intent(HomEvents.this,cultural.class);
        startActivity(i);
    }

    public  void instructions(View V)
    {
        Intent i=new Intent(HomEvents.this,registrationinstructions.class);
        startActivity(i);

    }
    public void gotofbs(View v )
    {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/mayukh.2k18"));
        startActivity(browserIntent);
    }

    public  void gotoinstas(View V) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.instagram.com/mayukh.2k18"));
        startActivity(browserIntent);

    }
}
