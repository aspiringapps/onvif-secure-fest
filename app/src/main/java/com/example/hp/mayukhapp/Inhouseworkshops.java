package com.example.hp.mayukhapp;

import android.database.DataSetObserver;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.Toast;

import org.w3c.dom.ls.LSException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Inhouseworkshops extends AppCompatActivity {

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;

    HashMap<String, List<String>> listDataChild;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inhouseworkshops);
        // get the listview
        expListView = (ExpandableListView) findViewById(R.id.lvExp);

        // preparing list data
        prepareListData();



            listAdapter = new expandablelistadapter(this,listDataHeader, listDataChild);

            // setting list adapter
            expListView.setAdapter(listAdapter);

            // Listview Group click listener
        expListView.setOnGroupClickListener(new OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView parent, View v,
                int groupPosition, long id) {
                    // Toast.makeText(getApplicationContext(),
                    // "Group Clicked " + listDataHeader.get(groupPosition),
                    // Toast.LENGTH_SHORT).show();
                    return false;
                }
            });

            // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new OnGroupExpandListener() {

                @Override
                public void onGroupExpand(int groupPosition) {
                }
            });

            // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new OnGroupCollapseListener() {

                @Override
                public void onGroupCollapse(int groupPosition) {

                }
            });

            // Listview on child click listener
            expListView.setOnChildClickListener(new OnChildClickListener() {

                @Override
                public boolean onChildClick(ExpandableListView parent, View v,
                int groupPosition, int childPosition, long id) {
                    // TODO Auto-generated method stub

                    return false;
                }
            });
        }

	/*
	 * Preparing the list data
	 */
    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding child data
        listDataHeader.add("CIM(Computer Integrated Manufaturing ");
        listDataHeader.add("Android App Development");
        listDataHeader.add("Latex");
        listDataHeader.add("Game Development using Unity3D");
        listDataHeader.add("Linux");
        listDataHeader.add("Drupal");
        listDataHeader.add("Aeromodelling");
        listDataHeader.add("Just Chocolate Things");
        listDataHeader.add("Dance");
        listDataHeader.add("Art");



        // Adding child data
        List<String> cim = new ArrayList<String>();
        cim.add("STUDENTS NAME:-");
        cim.add("Isha Varshney -- B.Tech 3rd Year (EI)");
        cim.add("Monika -- B.Tech 3rd Year (EI)");
        cim.add("Megha Dwivedi -- B.Tech 3rd Year (EI");
        cim.add("Ankita Kumari -- B.Tech 3rd Year (EI)");
        cim.add("Vishakha Srivastava -- B.Tech 3rd Year (EI)");
        cim.add("ASSIGNED TEACHER:-");
        cim.add("MR. Rajnish Kumar");
        cim.add("DATE - 25th March 2018");
        cim.add("DURATION - 4 hrs.");
        cim.add("CORE MEMBER ASSIGNED -");
        cim.add("Jaya Shukla -- B.Tech 4th Year (EC)");
        cim.add("SUB CORE MEMBER ASSIGNED -");
        cim.add("Shivi Mangal");





        List<String> android = new ArrayList<String>();
        android.add("STUDENTS NAME :-");
       android.add("Ridhima Bajpai -- B.Tech 4th year (CS)");
        android.add("Vibhuti -- B.Tech 3rd Year (CS)");
        android.add("Tanisha Awasthi -- B.Tech 4th Year (CS)");
        android.add("ASSIGNED TEACHER  :-");
        android.add("MRS. Deepanwita Thakur");
        android.add("DATE - 25th March 2018");
        android.add("DURATION -8 hrs");
        android.add("CORE MEMBER ASSIGNED -");
        android.add("Divyanshi Sharma -- M.Tech (VLSI)");
        android.add("SUB CORE MEMBER ASSIGNED - ");
        android.add("Priyanka Munjal");


        List<String> latex =new ArrayList<String>();
        latex.add("STUDENTS NAME :-");
        latex.add("Shilpa Gupta -- M.Phil (MATHS)");
        latex.add("Somi Gupta -- M.Sc Final (Maths)");
        latex.add("ASSIGNED TEACHER -");
        latex.add("MRS Gargi Tyagi");
        latex.add("DATE - 20th March 2018");
        latex.add("DURATION - 4 hrs");
        latex.add("CORE MEMBER ASSIGNED -");
       latex.add("Runika Dhar -- M.sc");
        latex.add("SUB CORE MEMBER ASSIGNED - ");
        latex.add("Vidushi Gupta");



        List<String> gamedevelopment =new ArrayList<String>();
       gamedevelopment.add("STUDENTS NAME :-");
        gamedevelopment.add("Drishti Sharma -- B.Tech 4th Year (CS)");
        gamedevelopment.add("Chakshu Jain -- B.Tech 4th Year (CS)");
         gamedevelopment.add("ASSIGN TEACHER  -");
     gamedevelopment.add("MR. Ashok Kumar");
        gamedevelopment.add("DATE - 25th March 2018");
        gamedevelopment.add("DURATION - 8 hrs");
        gamedevelopment.add("CORE MEMBER ASSIGNED -");
        gamedevelopment.add("Shreya B.Tech 3rd yr (EI)");
        gamedevelopment.add("SUB CORE MEMBER ASSIGNED - ");
        gamedevelopment.add("Vidushi Gupta");


        List<String> linux =new ArrayList<String >();
       linux.add("STUDENTS NAME :-");
        linux.add("Nandini -- M.Tech(CS)");
        linux.add("ASSIGNED TEACHER -");
      linux.add("DR. Sanjay Kumar Sharma");
        linux.add("DATE - 20th March 2018");
    linux.add("DURATION - 2 hrs");
        linux.add("CORE MEMBER ASSIGNED -");
        linux.add("Ambika Mehrotra B.Tech 4th yr (CS)");
        linux.add("SUB CORE MEMBER ASSIGNED - ");
       linux.add("Archita Ohri");


        List<String >dropal =new ArrayList<String >();
       dropal.add("STUDENTS NAME :-");
        dropal.add("Mrinalini Mishra -- B.Tech 4th Year (CS)");
      dropal.add("ASSIGN TEACHER -");
       dropal.add("MR. Manjeet Kumar");
        dropal.add("DATE - 21st March 2018");
        dropal.add("DURATION - 4 hrs");
        dropal.add("CORE MEMBER ASSIGNED -");
       dropal.add("Gauri Sharma B.Tech 4th yr (EC)");
        dropal.add("SUB CORE MEMBER ASSIGNED - ");
       dropal.add("Shivi Mangal");




        List<String> areomodelling =new ArrayList<String>();

      areomodelling.add("Mr Phugharasan -- Faculty");
        areomodelling.add("DATE - 25th March 2018");
        areomodelling.add("DURATION - 3 hrs");
        areomodelling.add("CORE MEMBER ASSIGN -");
       areomodelling.add("Nalini Swaraj B.Tech 4th yr (CS)");
       areomodelling.add("SUB CORE MEMBER ASSIGN - ");
        areomodelling.add("Dewanasha Gaur");




        List<String>justchocolate =new ArrayList<String>();
        justchocolate.add("STUDENTS NAME -");
        justchocolate.add("Muskaan --B.Tech 3rd Year(EI)");
        justchocolate.add("Priyanka Garg -- B.Tech 3rd Year (EI");
        justchocolate.add("Ritika Garg --B.Tech 3rd Year (EI)");
       justchocolate.add("DATE - 20th March 2018");
        justchocolate.add("DURATION - 2 hrs");
        justchocolate.add("CORE MEMBER ASSIGNED -");
        justchocolate.add("Gauri Sharma B.Tech 4th yr (EC)");
       justchocolate.add("SUB CORE MEMBER ASSIGNED - ");
        justchocolate.add("Sakshi Guptal");


        List<String>dance=new ArrayList<String>();
        dance.add("STUDENTS NAME -");
        dance.add("Kreety Sharma --B.Tech 4th Year(EC)");
        dance.add("Shivangi Sharma --B.Tech 3rd Year(CS)");
        dance.add("Kajol Jain --B.Tech 4th Year(CS)");
       dance.add("DATE - 25th March 2018");
        dance.add("DURATION - 3 hrs");
        dance.add("CORE MEMBER ASSIGNED -");
       dance.add("Ashmita  M.Sc Final Year");
       dance.add("SUB CORE MEMBER ASSIGNED - ");
        dance.add("Archita Ohri");


        List<String> art=new ArrayList<String>();
        art.add("STUDENTS NAME");
        art.add("Hakha Srivastava and Team -- B.Tech 3rd Year");
        art.add("DATE - 20th March 2018");
       art.add("DURATION - 2 hrs");
        art.add("CORE MEMBER ASSIGNED -");
       art.add("Hansa B.sc Final Year");
      art.add("SUB CORE MEMBER ASSIGNED - ");
        art.add("Priyanka Munjal");



        listDataChild.put(listDataHeader.get(0), cim); // Header, Child data
        listDataChild.put(listDataHeader.get(1), android);
        listDataChild.put(listDataHeader.get(2),latex);
        listDataChild.put(listDataHeader.get(3),gamedevelopment);
        listDataChild.put(listDataHeader.get(4),linux);
        listDataChild.put(listDataHeader.get(5),dropal);
        listDataChild.put(listDataHeader.get(6),areomodelling);
        listDataChild.put(listDataHeader.get(7),justchocolate);
        listDataChild.put(listDataHeader.get(8),dance);
        listDataChild.put(listDataHeader.get(9),art);
    }
}
