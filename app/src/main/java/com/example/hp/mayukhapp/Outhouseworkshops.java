package com.example.hp.mayukhapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Outhouseworkshops extends AppCompatActivity {

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;

    HashMap<String, List<String>> listDataChild;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outhouseworkshops);
        // get the listview
        expListView = (ExpandableListView) findViewById(R.id.lvExp);

        // preparing list data
        prepareListData();



        listAdapter = new expandablelistadapter(this,listDataHeader, listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);

        // Listview Group click listener
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                // Toast.makeText(getApplicationContext(),
                // "Group Clicked " + listDataHeader.get(groupPosition),
                // Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
            }
        });

        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {

            }
        });

        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub

                return false;
            }
        });
    }

    /*
     * Preparing the list data
     */
    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding child data
        listDataHeader.add("VEDIC MATHEMATICS");
        listDataHeader.add("BLUETOOTH CONTROL SPY ROBOT");
        listDataHeader.add("IOT");
        listDataHeader.add("CYBER DISEASE");
        listDataHeader.add("BLOCK CHAIN TECHNOLOGY");
        listDataHeader.add("MACHINE LEARNING WITH PYTHON");




        // Adding child data
        List<String> vm = new ArrayList<String>();
        vm.add("COMPANY NAME:-VEDIC MATHEMATICS");
        vm.add("PERSON NAME -Naveen Bhargava");
        vm.add("DATE - 20th March 2018");
        vm.add("DURATION - 3 hrs ");
        vm.add("STUDENT COORDINATOR -");
        vm.add("Asmita Shukla(Msc Maths)");
        vm.add("STUDENT DETAILS(MOBILE NO.)");
        vm.add("8009609327");
        vm.add("STUDENT DETAILS(EMAIL ID)");
        vm.add("asmishukla23@gmail.com");
        vm.add("SUB CORE ASSIGN -");
        vm.add("Devansha Gaur");


        List<String>bluetooth = new ArrayList<String>();
       bluetooth.add("COMPANY NAME:- EXACKT TECHNOLOGY JAIPUR");
        bluetooth.add("PERSON NAME - Peeyush Sanam");
        bluetooth.add("DATE - 26-27th March");
       bluetooth.add("DURATION - 13 hrs");
        bluetooth.add("STUDENT COORDINATOR -");
       bluetooth.add("Gauri Sharma(EC 4th year)");
        bluetooth.add("STUDENT DETAILS(MOBILE NO.)");
        bluetooth.add("7055226370");
        bluetooth.add("STUDENT DETAILS(EMAIL ID)");
        bluetooth.add("gauri031996m@gmail.com");
        bluetooth.add("Shreya  Indoliya(EI 3rd year)");
        bluetooth.add("STUDENT DETAILS(MOBILE NO.)");
        bluetooth.add("9997839425");
       bluetooth.add("STUDENT DETAILS(EMAIL ID)");
        bluetooth.add("shreyaindoliya34@gmail.com");
       bluetooth.add("SUB CORE ASSIGN -");
       bluetooth.add("Sakshi Gupta");



        List<String> iot = new ArrayList<String>();
      iot.add("COMPANY NAME:- D'CYBORG");
       iot.add("PERSON NAME - Durga Prasad Yadav");
       iot.add("DATE - 27th March");
        iot.add("DURATION - 7-8 hrs");
       iot.add("STUDENT COORDINATOR -");
        iot.add("Divyanshi Sharma(Mtech VLSI)");
     iot.add("STUDENT DETAILS(MOBILE NO.)");
     iot.add("7378125657");
   iot.add("STUDENT DETAILS(EMAIL ID)");
        iot.add("sharma.divyanshi47@gmail.com");
       iot.add("Shivani Prasad(IT 3rd year)");
    iot.add("STUDENT DETAILS(MOBILE NO.)");
    iot.add("7023663848");
      iot.add("STUDENT DETAILS(EMAIL ID)");
       iot.add("prasadshivani165@gmail.com");
       iot.add("SUB CORE ASSIGN -");
      iot.add("Archita Ohri");



        List<String> cyberdisease =new ArrayList<String>();
        cyberdisease.add("COMPANY NAME:- I-3 INDYA");
        cyberdisease.add("PERSON NAME - Chamkaur Singh");
        cyberdisease.add("DATE - 26-27th March");
        cyberdisease.add("DURATION - 14hrs");
        cyberdisease.add("STUDENT COORDINATOR -");
        cyberdisease.add("Nalini Swaraj(CS 4th year)");
      cyberdisease.add("STUDENT DETAILS(MOBILE NO.)");
        cyberdisease.add("8426083882");
       cyberdisease.add("STUDENT DETAILS(EMAIL ID)");
      cyberdisease.add("naliniswaraj1997@gmail.com");
       cyberdisease.add("Hansa(Bsc. Final year)");
       cyberdisease.add("STUDENT DETAILS(MOBILE NO.)");
       cyberdisease.add("9958558440");
       cyberdisease.add("STUDENT DETAILS(EMAIL ID)");
      cyberdisease.add("furious.hansa@gmail.com");
       cyberdisease.add("SUB CORE ASSIGN -");
       cyberdisease.add("Shivi Mangal");




        List<String> blockchain =new ArrayList<String>();
      blockchain.add("COMPANY NAME:- GREEKSLAB");
      blockchain.add("PERSON NAME - Vaibhav Gupta");
        blockchain.add("DATE - 26-27th March 2018");
        blockchain.add("DURATION - 16 hrs");
      blockchain.add("STUDENT COORDINATOR -");
        blockchain.add("Ambika Mehrotra(CS 4th year)");
       blockchain.add("STUDENT DETAILS(MOBILE NO.)");
      blockchain.add("7880440029");
       blockchain.add("STUDENT DETAILS(EMAIL ID)");
       blockchain.add("ambikamehrotra1211@gmail.com");
    blockchain.add("Runika Dhar(MCA L) ");
       blockchain.add("STUDENT DETAILS(MOBILE NO.)");
     blockchain.add("7232010700");
      blockchain.add("STUDENT DETAILS(EMAIL ID)");
        blockchain.add("runikhadhar@gmail.com");
      blockchain.add("SUB CORE ASSIGN -");
        blockchain.add("Priyanka Munjal");




        List<String> machinelearn =new ArrayList<String>();
        machinelearn.add("COMPANY NAME:- TECHIENEST");
        machinelearn.add("PERSON NAME - Siddarth Singh ");
        machinelearn.add("DATE - 27th March 2018");
        machinelearn.add("DURATION -  7-8 hrs");
        machinelearn.add("STUDENT COORDINATOR -");
        machinelearn.add("Asmita (Msc Maths)");
        machinelearn.add("STUDENT DETAILS(MOBILE NO.)");
        machinelearn.add("8009609327");
        machinelearn.add("STUDENT DETAILS(EMAIL ID)");
        machinelearn.add("asmishukla23@gmail.com");
        machinelearn.add("Jaya Shukla (EC 4th year)");
        machinelearn.add("STUDENT DETAILS(MOBILE NO.)");
        machinelearn.add("7073210723");
        machinelearn.add("STUDENT DETAILS(EMAIL ID)");
        machinelearn.add("jayashukla12345@gmail.com");
        machinelearn.add("SUB CORE ASSIGN -");
        machinelearn.add("Divansha Gaur");




        listDataChild.put(listDataHeader.get(0),vm); // Header, Child data
        listDataChild.put(listDataHeader.get(1), bluetooth);
        listDataChild.put(listDataHeader.get(2), iot);
        listDataChild.put(listDataHeader.get(3),cyberdisease);
        listDataChild.put(listDataHeader.get(4),blockchain);
        listDataChild.put(listDataHeader.get(5),machinelearn);

    }
}

