package com.example.hp.mayukhapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.net.Uri;
import android.view.Window;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

public class Videoplay extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videoplay);


        VideoView vidView = (VideoView)findViewById(R.id.myVideo);

        MediaController vidControl = new MediaController(this);

        String vidAddress = "https://archive.org/download/ksnn_compilation_master_the_internet/ksnn_compilation_master_the_internet_512kb.mp4";
        Uri vidUri = Uri.parse(vidAddress);

        Toast.makeText(Videoplay.this, "Loading Teaser",Toast.LENGTH_LONG).show();

        vidView.setVideoURI(vidUri);

        vidView.start();


        vidControl.setAnchorView(vidView);

        vidView.setMediaController(vidControl);


    }
}
